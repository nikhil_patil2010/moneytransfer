# moneytransfer


## How to Run?

### Requirements
* Java 8
* Maven 3

### Get the source
```
git clone https://nikhil_patil2010@bitbucket.org/nikhil_patil2010/moneytransfer.git
cd moneytransfer
```

### Run Tests

```
mvn test 
```

### Start Application

```
mvn clean install
java -jar moneytransfer-core/target/moneytransfer.jar server moneytransfer-core/src/main/resources/config.yml
```


## API Docs: 
https://documenter.getpostman.com/view/99521/SVfQRUXk

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/a26a46a99db27b913caa)