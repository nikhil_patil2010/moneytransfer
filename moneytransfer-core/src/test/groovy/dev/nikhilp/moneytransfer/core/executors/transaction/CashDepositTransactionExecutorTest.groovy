package dev.nikhilp.moneytransfer.core.executors.transaction

import dev.nikhilp.moneytransfer.core.dal.dao.AccountDAO
import dev.nikhilp.moneytransfer.core.dal.dao.TransactionDAO
import dev.nikhilp.moneytransfer.core.dal.transaction.DBContext
import dev.nikhilp.moneytransfer.core.dal.transaction.DBTransactionHelper
import dev.nikhilp.moneytransfer.core.exceptions.TransactionException
import dev.nikhilp.moneytransfer.core.models.data.Account
import dev.nikhilp.moneytransfer.core.models.data.Transaction
import dev.nikhilp.moneytransfer.core.models.data.TransactionResponseCode
import dev.nikhilp.moneytransfer.core.models.data.TransactionType
import spock.lang.Specification

class CashDepositTransactionExecutorTest extends Specification {
    CashDepositTransactionExecutor cashDepositTransactionExecutor

    def setup() {
        DBTransactionHelper dbTransactionHelper = Stub(DBTransactionHelper.class)
        TransactionDAO transactionDAO = Stub(TransactionDAO)
        AccountDAO accountDAO = Stub(AccountDAO.class)
        cashDepositTransactionExecutor = new CashDepositTransactionExecutor(dbTransactionHelper, transactionDAO, accountDAO)
    }

    def "Test process for valid account_id and amount"() {
        when: "valid transaction request"
        Transaction transaction = new Transaction()
        transaction.setDebitAccountId("123")
        transaction.setTransactionType(TransactionType.CASH_DEPOSIT)
        transaction.setAmount(50)

        and: "accountDAO returns account"
        Account account = new Account()
        account.accountId ="123"
        account.accountBalance = 100

        cashDepositTransactionExecutor.accountDAO.selectAccountForUpdate(_,_) >> account

        and: "process is called"
        cashDepositTransactionExecutor.process(new DBContext(), transaction)

        then: "Deposit is successful"
        account.accountBalance == 150
    }

    def "Test process for invalid account_id and valid amount"() {
        when: "valid transaction request"
        Transaction transaction = new Transaction()
        transaction.setDebitAccountId("123")
        transaction.setTransactionType(TransactionType.CASH_DEPOSIT)
        transaction.setAmount(50)

        and: "accountDAO doesn't return any account"
        cashDepositTransactionExecutor.accountDAO.selectAccountForUpdate(_,_) >> null

        and: "process is called"
        cashDepositTransactionExecutor.process(new DBContext(), transaction)

        then: "transfer is failed"
        TransactionException e = thrown(TransactionException)
        e.transactionResponseCode == TransactionResponseCode.ACCOUNT_DOES_NOT_EXIST
    }
}
