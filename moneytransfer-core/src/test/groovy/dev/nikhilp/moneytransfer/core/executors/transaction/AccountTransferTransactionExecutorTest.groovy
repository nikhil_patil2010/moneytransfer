package dev.nikhilp.moneytransfer.core.executors.transaction

import dev.nikhilp.moneytransfer.core.dal.dao.AccountDAO
import dev.nikhilp.moneytransfer.core.dal.dao.TransactionDAO
import dev.nikhilp.moneytransfer.core.dal.transaction.DBContext
import dev.nikhilp.moneytransfer.core.dal.transaction.DBTransactionHelper
import dev.nikhilp.moneytransfer.core.exceptions.TransactionException
import dev.nikhilp.moneytransfer.core.models.data.Account
import dev.nikhilp.moneytransfer.core.models.data.Transaction
import dev.nikhilp.moneytransfer.core.models.data.TransactionResponseCode
import dev.nikhilp.moneytransfer.core.models.data.TransactionType
import spock.lang.Specification

class AccountTransferTransactionExecutorTest extends Specification {

    AccountTransferTransactionExecutor accountTransferTransactionExecutor

    def setup() {
        DBTransactionHelper dbTransactionHelper = Stub(DBTransactionHelper.class)
        TransactionDAO transactionDAO = Stub(TransactionDAO)
        AccountDAO accountDAO = Stub(AccountDAO.class)
        accountTransferTransactionExecutor = new AccountTransferTransactionExecutor(dbTransactionHelper, transactionDAO, accountDAO)
    }

    def "Test process for valid account_id and amount"() {
        when: "valid transaction request"
        Transaction transaction = new Transaction()
        transaction.setDebitAccountId("123")
        transaction.setCreditAccountId("789")
        transaction.setTransactionType(TransactionType.ACCOUNT_TRANSFER)
        transaction.setAmount(50)

        and: "accountDAO returns both source and destination accounts"
        Account sourceAccount = new Account()
        sourceAccount.accountId ="123"
        sourceAccount.accountBalance = 100

        Account destinationAccount = new Account()
        destinationAccount.accountId ="789"
        destinationAccount.accountBalance = 10

        accountTransferTransactionExecutor.accountDAO.selectAccountsForUpdate(_,_) >> Arrays.asList(sourceAccount, destinationAccount)

        and: "process is called"
        accountTransferTransactionExecutor.process(new DBContext(), transaction)

        then: "transfer is successful"
        sourceAccount.accountBalance == 50
        destinationAccount.accountBalance == 60

    }

    def "Test process for valid source and invalid dest account_id and amount"() {
        when: "valid transaction request"
        Transaction transaction = new Transaction()
        transaction.setDebitAccountId("123")
        transaction.setCreditAccountId("789")
        transaction.setTransactionType(TransactionType.ACCOUNT_TRANSFER)
        transaction.setAmount(50)

        and: "accountDAO returns both source and destination accounts"
        Account sourceAccount = new Account()
        sourceAccount.accountId ="123"
        sourceAccount.accountBalance = 100

        accountTransferTransactionExecutor.accountDAO.selectAccountsForUpdate(_,_) >> Arrays.asList(sourceAccount)

        and: "process is called"
        accountTransferTransactionExecutor.process(new DBContext(), transaction)

        then: "transfer is failed with ACCOUNT_DOES_NOT_EXIST"
        TransactionException e = thrown(TransactionException)
        e.transactionResponseCode == TransactionResponseCode.ACCOUNT_DOES_NOT_EXIST
        sourceAccount.accountBalance == 100

    }

    def "Test process for invalid source and valid dest account_id and amount"() {
        when: "valid transaction request"
        Transaction transaction = new Transaction()
        transaction.setDebitAccountId("123")
        transaction.setCreditAccountId("789")
        transaction.setTransactionType(TransactionType.ACCOUNT_TRANSFER)
        transaction.setAmount(50)

        and: "accountDAO returns both source and destination accounts"
        Account destAccount = new Account()
        destAccount.accountId ="789"
        destAccount.accountBalance = 10

        accountTransferTransactionExecutor.accountDAO.selectAccountsForUpdate(_,_) >> Arrays.asList(destAccount)

        and: "process is called"
        accountTransferTransactionExecutor.process(new DBContext(), transaction)

        then: "transfer is failed with ACCOUNT_DOES_NOT_EXIST"
        TransactionException e = thrown(TransactionException)
        e.transactionResponseCode == TransactionResponseCode.ACCOUNT_DOES_NOT_EXIST
        destAccount.accountBalance == 10

    }

    def "Test process for valid account_id and amount more than available balance in source"() {
        when: "valid transaction request"
        Transaction transaction = new Transaction()
        transaction.setDebitAccountId("123")
        transaction.setCreditAccountId("789")
        transaction.setTransactionType(TransactionType.ACCOUNT_TRANSFER)
        transaction.setAmount(150)

        and: "accountDAO returns both source and destination accounts"
        Account sourceAccount = new Account()
        sourceAccount.accountId ="123"
        sourceAccount.accountBalance = 100

        Account destinationAccount = new Account()
        destinationAccount.accountId ="789"
        destinationAccount.accountBalance = 10

        accountTransferTransactionExecutor.accountDAO.selectAccountsForUpdate(_,_) >> Arrays.asList(sourceAccount, destinationAccount)

        and: "process is called"
        accountTransferTransactionExecutor.process(new DBContext(), transaction)

        then: "transfer is failed"
        TransactionException e = thrown(TransactionException)
        e.transactionResponseCode == TransactionResponseCode.INSUFFICIENT_BALANCE
        sourceAccount.accountBalance == 100
        destinationAccount.accountBalance == 10

    }
}
