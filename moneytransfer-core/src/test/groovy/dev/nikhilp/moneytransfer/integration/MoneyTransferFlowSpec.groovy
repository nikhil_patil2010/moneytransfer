package dev.nikhilp.moneytransfer.integration

import com.fasterxml.jackson.databind.ObjectMapper
import dev.nikhilp.moneytransfer.core.MoneyTransferApplication
import dev.nikhilp.moneytransfer.core.MoneyTransferConfiguration
import dev.nikhilp.moneytransfer.core.models.data.AccountStatus
import dev.nikhilp.moneytransfer.core.models.data.AccountType
import dev.nikhilp.moneytransfer.core.models.data.TransactionResponseCode
import dev.nikhilp.moneytransfer.core.models.data.TransactionStatus
import dev.nikhilp.moneytransfer.core.models.data.TransactionType
import dev.nikhilp.moneytransfer.core.models.request.CreateAccountRequest
import dev.nikhilp.moneytransfer.core.models.request.CreateUserRequest
import dev.nikhilp.moneytransfer.core.models.request.TransactionRequest
import dev.nikhilp.moneytransfer.core.models.response.AccountResponse
import dev.nikhilp.moneytransfer.core.models.response.TransactionResponse
import dev.nikhilp.moneytransfer.core.models.response.UserResponse
import io.dropwizard.client.JerseyClientBuilder
import io.dropwizard.testing.DropwizardTestSupport
import io.dropwizard.testing.ResourceHelpers
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.Shared
import spock.lang.Specification

import javax.ws.rs.client.Client
import javax.ws.rs.client.Entity
import javax.ws.rs.core.Response

import static io.dropwizard.testing.FixtureHelpers.fixture

class MoneyTransferFlowSpec extends Specification {
    @Shared Logger logger

    public static final DropwizardTestSupport<MoneyTransferConfiguration> SUPPORT =
            new DropwizardTestSupport<MoneyTransferConfiguration>(MoneyTransferApplication.class,
                    ResourceHelpers.resourceFilePath("config.yml"));
    @Shared Client client
    @Shared UserResponse user1
    @Shared UserResponse user2
    @Shared ObjectMapper objectMapper


    void setupSpec() {
        logger = LoggerFactory.getLogger(MoneyTransferFlowSpec.class);
        SUPPORT.before()
        client = new JerseyClientBuilder(SUPPORT.getEnvironment())
                .using(SUPPORT.getConfiguration().jerseyClientConfiguration)
                .build("test client");

        objectMapper = new ObjectMapper()
        user1 = createUser("fixtures/account/create_user1.json")
        user2 = createUser("fixtures/account/create_user2.json")
    }

    void cleanupSpec() {
        client.close()
        SUPPORT.after()
    }

    def "Test account creation on valid user id"() {
        logger.info("Test account creation on valid user id")
        when: "trying to create account on valid user id"
        AccountResponse createAccountResponse = createSavingsAccount(user1.getId())

        then: "Account creation is successful"
        createAccountResponse.accountStatus == AccountStatus.ACTIVE
    }

    def "Test account creation on invalid user id"() {
        logger.info("Test account creation on invalid user id")
        when: "trying to create account on invalid user id"
        CreateAccountRequest createAccountRequest = new CreateAccountRequest()
        createAccountRequest.setUserId("test123")
        createAccountRequest.setAccountType(AccountType.SAVINGS)

        Response response = client.target(
                String.format("http://localhost:%d/account/create", SUPPORT.getLocalPort()))
                .request()
                .post(Entity.json(createAccountRequest));

        then: "Account creation is failed with bad request"
        response.status == 400

    }

    def "Test account transfer transaction with correct amount and correct account ids"() {
        logger.info("Test account transfer transaction with correct amount and correct account ids")
        when: "account 1 with amount 100"
        AccountResponse createAccountResponse1 = createSavingsAccount(user1.getId())
        deposit(100.0, createAccountResponse1.accountId)

        and: "account 2"
        AccountResponse createAccountResponse2 = createSavingsAccount(user2.getId())

        and: "Account 1 tries to transfer 50 to Account 2"
        TransactionResponse transactionResponse = transfer(50.0, createAccountResponse1.accountId, createAccountResponse2.accountId)

        then: "Transaction is successful and both account's balance is 50 each"
        transactionResponse.status == TransactionStatus.SUCCESS
    }


    def "Test account transfer transaction with more amount than balance and correct account ids"() {
        logger.info("Test account transfer transaction with more amount than balance and correct account ids")
        when: "account 1 with amount 100"
        AccountResponse createAccountResponse1 = createSavingsAccount(user1.getId())
        deposit(100.0, createAccountResponse1.accountId)

        and: "account 2"
        AccountResponse createAccountResponse2 = createSavingsAccount(user2.getId())

        and: "Account 1 tries to transfer 150 to Account 2"
        TransactionResponse transactionResponse = transfer(150.0, createAccountResponse1.accountId, createAccountResponse2.accountId)

        then: "Transaction is successful and both account's balance is 50 each"
        transactionResponse.status == TransactionStatus.FAILED
        transactionResponse.responseCode == TransactionResponseCode.INSUFFICIENT_BALANCE
    }

    def "Test account transfer transaction with correct amount and invalid source account id"() {
        logger.info("Test account transfer transaction with more amount than balance and correct account ids")
        when: "account 1 with amount 100"
        AccountResponse createAccountResponse1 = createSavingsAccount(user1.getId())
        deposit(100.0, createAccountResponse1.accountId)

        and: "Account 1 tries to transfer 100 to non existent"
        TransactionResponse transactionResponse = transfer(150.0, "dummy", createAccountResponse1.accountId)

        then: "Transaction is successful and both account's balance is 50 each"
        transactionResponse.status == TransactionStatus.FAILED
        transactionResponse.responseCode == TransactionResponseCode.ACCOUNT_DOES_NOT_EXIST
    }

    def "Test account transfer transaction with correct amount and invalid destination account id"() {
        logger.info("Test account transfer transaction with more amount than balance and correct account ids")
        when: "account 1 with amount 100"
        AccountResponse createAccountResponse1 = createSavingsAccount(user1.getId())
        deposit(100.0, createAccountResponse1.accountId)

        and: "Account 1 tries to transfer 100 to non existent"
        TransactionResponse transactionResponse = transfer(150.0, createAccountResponse1.accountId, "dummy")

        then: "Transaction is successful and both account's balance is 50 each"
        transactionResponse.status == TransactionStatus.FAILED
        transactionResponse.responseCode == TransactionResponseCode.ACCOUNT_DOES_NOT_EXIST
    }

    UserResponse createUser(String requestJson) {
        CreateUserRequest createUserRequest = objectMapper.readValue(fixture(requestJson), CreateUserRequest.class)

        Response response = client.target(
                String.format("http://localhost:%d/user/create", SUPPORT.getLocalPort()))
                .request()
                .post(Entity.json(createUserRequest));

        UserResponse userResponse = objectMapper.readValue(response.readEntity(String.class), UserResponse.class)
        logger.info(userResponse.toString())
        return userResponse
    }

    AccountResponse createSavingsAccount(String userId) {
        CreateAccountRequest createAccountRequest = new CreateAccountRequest()
        createAccountRequest.setUserId(userId)
        createAccountRequest.setAccountType(AccountType.SAVINGS)

        Response response = client.target(
                String.format("http://localhost:%d/account/create", SUPPORT.getLocalPort()))
                .request()
                .post(Entity.json(createAccountRequest));

        AccountResponse createAccountResponse = objectMapper.readValue(response.readEntity(String.class), AccountResponse.class)
        logger.info(createAccountResponse.toString())
        return createAccountResponse
    }

    TransactionResponse deposit(Double amount, String accountId) {
        TransactionRequest transactionRequest = new TransactionRequest()
        transactionRequest.setTransactionType(TransactionType.CASH_DEPOSIT)
        transactionRequest.setAmount(amount)
        transactionRequest.setCreditAccountId(accountId)

        Response response = client.target(
                String.format("http://localhost:%d/transaction/execute", SUPPORT.getLocalPort()))
                .request()
                .post(Entity.json(transactionRequest));

        TransactionResponse transactionResponse = objectMapper.readValue(response.readEntity(String.class), TransactionResponse.class)
        logger.info(transactionResponse.toString())
        return transactionResponse
    }

    TransactionResponse transfer(Double amount, String sourceAccountId, String destinationAccountId) {
        logger.info("Transferring {} from account {} to account {}", amount, sourceAccountId, destinationAccountId)
        TransactionRequest transactionRequest = new TransactionRequest()
        transactionRequest.setTransactionType(TransactionType.ACCOUNT_TRANSFER)
        transactionRequest.setAmount(amount)
        transactionRequest.setDebitAccountId(sourceAccountId)
        transactionRequest.setCreditAccountId(destinationAccountId)

        Response response = client.target(
                String.format("http://localhost:%d/transaction/execute", SUPPORT.getLocalPort()))
                .request()
                .post(Entity.json(transactionRequest));

        TransactionResponse transactionResponse = objectMapper.readValue(response.readEntity(String.class), TransactionResponse.class)
        logger.info("Transaction Response {} ",transactionResponse.toString())
        return transactionResponse
    }
}
