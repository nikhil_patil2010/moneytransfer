package dev.nikhilp.moneytransfer.core.dal.dao.mysqlImpl;

import dev.nikhilp.moneytransfer.core.dal.JdbiMysqlDAO;
import dev.nikhilp.moneytransfer.core.dal.dao.TransactionDAO;
import dev.nikhilp.moneytransfer.core.dal.dao.handles.TransactionDAOHandle;
import dev.nikhilp.moneytransfer.core.dal.sessionhandle.Session;
import dev.nikhilp.moneytransfer.core.models.data.Transaction;

import java.util.List;

public class TransactionDAOMysqlImpl extends JdbiMysqlDAO<TransactionDAOHandle> implements TransactionDAO {
    @Override
    public void createTransaction(Transaction transaction) {
        getSessionHandleOnDemand().createTransaction(transaction);
    }

    @Override
    public void updateTransactionStatusAndResponseCode(Transaction transaction, Session session) {
        getSessionHandleFromSession(session).updateTransactionStatusAndResponseCode(transaction);
    }

    @Override
    public void updateTransactionStatusAndResponseCode(Transaction transaction) {
        getSessionHandleOnDemand().updateTransactionStatusAndResponseCode(transaction);
    }

    @Override
    public List<Transaction> getTransactions(){
        return getSessionHandleOnDemand().getTransactions();
    }
}
