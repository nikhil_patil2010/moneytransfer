package dev.nikhilp.moneytransfer.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.client.HttpClientConfiguration;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.db.DataSourceFactory;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MoneyTransferConfiguration extends Configuration {

    @NotNull
    @JsonProperty("database")
    private DataSourceFactory database = new DataSourceFactory();

    @JsonProperty("jerseyClient")
    private JerseyClientConfiguration jerseyClientConfiguration;
}
