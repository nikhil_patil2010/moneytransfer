package dev.nikhilp.moneytransfer.core.dal.dao.mappers;

import dev.nikhilp.moneytransfer.core.models.data.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements ResultSetMapper<User> {
    @Override
    public User map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return User.builder().id(resultSet.getString("id"))
                .name(resultSet.getString("name"))
                .address(resultSet.getString("address"))
                .dateOfBirth(resultSet.getTimestamp("date_of_birth"))
                .gender(Gender.valueOf(resultSet.getString("gender")))
                .nationalId(resultSet.getString("national_id"))
                .createdAt(resultSet.getTimestamp("created_at"))
                .updatedAt(resultSet.getTimestamp("updated_at"))
                .build();
    }
}
