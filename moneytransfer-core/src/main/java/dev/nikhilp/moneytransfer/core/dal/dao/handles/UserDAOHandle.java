package dev.nikhilp.moneytransfer.core.dal.dao.handles;

import dev.nikhilp.moneytransfer.core.dal.dao.mappers.UserMapper;
import dev.nikhilp.moneytransfer.core.dal.sessionhandle.SessionHandle;
import dev.nikhilp.moneytransfer.core.models.data.User;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.GetHandle;

import java.util.List;

public abstract class UserDAOHandle implements GetHandle, SessionHandle {

    @SqlUpdate("INSERT into users (id, name, date_of_birth, gender, address, national_id, created_at, updated_at)" +
            " values (:id, :name, :dateOfBirth, :gender, :address, :nationalId, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
    public abstract void createUser(@BindBean User user);

    @SqlQuery("SELECT * FROM users WHERE id = :id")
    @RegisterMapper(UserMapper.class)
    public abstract User getUser(@Bind("id") String id);

    @SqlQuery("SELECT * FROM users LIMIT 10")
    @RegisterMapper(UserMapper.class)
    public abstract List<User> getUsers();
}
