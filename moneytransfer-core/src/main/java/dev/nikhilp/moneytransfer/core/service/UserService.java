package dev.nikhilp.moneytransfer.core.service;

import com.google.inject.Inject;
import dev.nikhilp.moneytransfer.core.dal.dao.UserDAO;
import dev.nikhilp.moneytransfer.core.models.data.User;
import dev.nikhilp.moneytransfer.core.models.request.CreateUserRequest;
import dev.nikhilp.moneytransfer.core.models.response.UserResponse;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
public class UserService {

    private final UserDAO userDAO;

    @Inject
    public UserService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public UserResponse createUser(CreateUserRequest createUserRequest) {
        User user = adaptRequest(createUserRequest);
        user.setId(generateUserId());
        userDAO.createUser(user);
        log.info("User creation successful for request {}", createUserRequest);
        return adaptToResponse(user);
    }

    public UserResponse getUser(String userId) {
        return adaptToResponse(userDAO.getUser(userId));
    }

    public List<UserResponse> getUsers() {
        return userDAO.getUsers().stream().map(this::adaptToResponse).collect(Collectors.toList());
    }

    private User adaptRequest(CreateUserRequest createUserRequest) {
        return User.builder().name(createUserRequest.getName())
                .dateOfBirth(createUserRequest.getDateOfBirth())
                .gender(createUserRequest.getGender())
                .address(createUserRequest.getAddress())
                .nationalId(createUserRequest.getNationalId())
                .build();
    }

    private UserResponse adaptToResponse(User user) {
        if(Objects.isNull(user)) return null;

        return UserResponse.builder().id(user.getId())
                .name(user.getName())
                .dateOfBirth(user.getDateOfBirth())
                .gender(user.getGender())
                .address(user.getAddress())
                .nationalId(user.getNationalId())
                .createdAt(user.getCreatedAt())
                .updatedAt(user.getUpdatedAt())
                .build();
    }

    private String generateUserId() {
        return UUID.randomUUID().toString();
    }
}
