package dev.nikhilp.moneytransfer.core.exceptions;

import dev.nikhilp.moneytransfer.core.models.data.TransactionResponseCode;
import lombok.Data;

public class TransactionException extends RuntimeException {

    private TransactionResponseCode transactionResponseCode;

    public TransactionException(String message, TransactionResponseCode transactionResponseCode) {
        super(message);
        this.transactionResponseCode = transactionResponseCode;
    }

    public TransactionResponseCode getTransactionResponseCode() {
        return transactionResponseCode;
    }
}
