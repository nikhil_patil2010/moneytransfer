package dev.nikhilp.moneytransfer.core.dal.sessionhandle;

import com.google.common.collect.Maps;
import org.reflections.Reflections;
import org.skife.jdbi.v2.Handle;

import java.util.Map;
import java.util.Set;

public class DBSession implements Session {
    private static Set<Class<? extends SessionHandle>> allSessionHandlers;

    static {
        Reflections sessionHandleReflections = new Reflections("dev.nikhilp.moneytransfer.core.dal.dao.handles");
        allSessionHandlers = sessionHandleReflections.getSubTypesOf(SessionHandle.class);
    }

    private final Handle handle;
    private Map<String, SessionHandle> attachedSessionHandleRegistry = Maps.newHashMap();

    public DBSession(Handle handle) {
        this.handle = handle;
        for(Class<? extends SessionHandle> sessionHandle : allSessionHandlers) {
            attachedSessionHandleRegistry.put(sessionHandle.getSimpleName(), this.handle.attach(sessionHandle));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public <SessionHandler extends SessionHandle> SessionHandler getSessionHandle(Class<SessionHandler> sessionHandlerClass) {
        return (SessionHandler) this.attachedSessionHandleRegistry.get(sessionHandlerClass.getSimpleName());
    }

    @SuppressWarnings("unchecked")
    @Override
    public <ConnectionHandler> ConnectionHandler getConnectionHandler() {
        return (ConnectionHandler) this.handle;
    }
}
