package dev.nikhilp.moneytransfer.core.resources;

import com.google.inject.Inject;
import dev.nikhilp.moneytransfer.core.models.request.TransactionRequest;
import dev.nikhilp.moneytransfer.core.models.response.TransactionResponse;
import dev.nikhilp.moneytransfer.core.service.TransactionService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TransactionResource {

    private final TransactionService transactionService;

    @Inject
    public TransactionResource(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @POST
    @Path("/execute")
    public Response executeTransaction(TransactionRequest transactionRequest) {
        TransactionResponse transactionResponse = transactionService.execute(transactionRequest);
        return Response.status(201).entity(transactionResponse).build();
    }

    @GET
    @Path("/")
    public Response getTransactions() {
        List<TransactionResponse> transactions = transactionService.get();
        return Response.status(200).entity(transactions).build();
    }
}
