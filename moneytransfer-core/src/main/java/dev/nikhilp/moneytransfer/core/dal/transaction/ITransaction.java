package dev.nikhilp.moneytransfer.core.dal.transaction;

public interface ITransaction {
    void doTransaction(DBContext context, ITransactionBlock iTransactionBlock);
}
