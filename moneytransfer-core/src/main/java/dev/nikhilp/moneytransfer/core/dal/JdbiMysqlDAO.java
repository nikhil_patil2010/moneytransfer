package dev.nikhilp.moneytransfer.core.dal;

import com.google.inject.Inject;
import dev.nikhilp.moneytransfer.core.dal.sessionhandle.Session;
import dev.nikhilp.moneytransfer.core.dal.sessionhandle.SessionHandle;
import org.skife.jdbi.v2.DBI;

import java.lang.reflect.ParameterizedType;

public class JdbiMysqlDAO<T extends SessionHandle> {
    @Inject
    private DBI jdbi;

    private Class<T> type;

    @SuppressWarnings("unchecked")
    public JdbiMysqlDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    protected T getSessionHandleOnDemand() {
        return this.jdbi.onDemand(this.type);
    }

    protected T getSessionHandleFromSession(Session session) {
            return session.getSessionHandle(this.type);
    }
}
