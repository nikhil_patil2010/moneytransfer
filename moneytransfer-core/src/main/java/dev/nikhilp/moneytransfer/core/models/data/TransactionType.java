package dev.nikhilp.moneytransfer.core.models.data;

public enum TransactionType {
    CASH_WITHDRAWL,
    CASH_DEPOSIT,
    ACCOUNT_TRANSFER
}
