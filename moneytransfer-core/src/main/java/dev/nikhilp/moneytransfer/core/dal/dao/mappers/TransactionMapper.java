package dev.nikhilp.moneytransfer.core.dal.dao.mappers;

import dev.nikhilp.moneytransfer.core.models.data.Transaction;
import dev.nikhilp.moneytransfer.core.models.data.TransactionResponseCode;
import dev.nikhilp.moneytransfer.core.models.data.TransactionStatus;
import dev.nikhilp.moneytransfer.core.models.data.TransactionType;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class TransactionMapper implements ResultSetMapper<Transaction> {
    @Override
    public Transaction map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return Transaction.builder()
                .transactionId(resultSet.getString("transaction_id"))
                .transactionType(TransactionType.valueOf(resultSet.getString("transaction_type")))
                .amount(resultSet.getDouble("amount"))
                .debitAccountId(resultSet.getString("debit_account_id"))
                .creditAccountId(resultSet.getString("credit_account_id"))
                .status(TransactionStatus.valueOf(resultSet.getString("status")))
                .responseCode(Objects.nonNull(resultSet.getObject("response_code"))?TransactionResponseCode.valueOf(resultSet.getString("response_code")):null)
                .createdAt(resultSet.getTimestamp("created_at"))
                .updatedAt(resultSet.getTimestamp("updated_at"))
                .build();
    }
}
