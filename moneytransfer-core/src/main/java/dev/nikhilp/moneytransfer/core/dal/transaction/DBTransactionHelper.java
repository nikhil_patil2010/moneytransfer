package dev.nikhilp.moneytransfer.core.dal.transaction;

import com.google.inject.Inject;
import dev.nikhilp.moneytransfer.core.dal.sessionhandle.DBSession;
import dev.nikhilp.moneytransfer.core.managers.MasterDBSessionManager;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.ServerErrorException;
import java.util.Objects;

@Slf4j
public class DBTransactionHelper extends DBTransaction implements ITransaction {

    @Inject
    MasterDBSessionManager dbSessionManager;

    @Override
    public void doTransaction(DBContext context, ITransactionBlock iTransactionBlock) {
        doTransactionInternal(new IDriven() {
            @Override
            public void drive(DBSession session) throws Exception {
                context.session = session;
                iTransactionBlock.execute(context);
            }
        });
    }

    private void doTransactionInternal(IDriven driven) {
        DBSession dbSession = this.dbSessionManager.getMasterDBSession();
        try {
            startTransaction(dbSession);
            driven.drive(dbSession);
            commit(dbSession);
        } catch (ServerErrorException e) {
            log.error("Exception during transaction", e);
            rollback(dbSession);
            throw e;
        } catch (Exception e) {
            log.error("Exception during transaction", e);
            rollback(dbSession);
            throw new RuntimeException("Exception during transaction", e);
        } finally {
            if(Objects.nonNull(dbSession)) {
                this.dbSessionManager.returnDBSession(dbSession);
            }
        }
    }

    private interface IDriven {
        void drive(DBSession session) throws Exception;
    }
}
