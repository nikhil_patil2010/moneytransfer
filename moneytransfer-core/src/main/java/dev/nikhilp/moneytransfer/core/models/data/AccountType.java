package dev.nikhilp.moneytransfer.core.models.data;

public enum AccountType {
    SAVINGS,
    CURRENT,
    DEPOSIT
}
