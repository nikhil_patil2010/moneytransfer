package dev.nikhilp.moneytransfer.core.models.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import dev.nikhilp.moneytransfer.core.models.data.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransactionRequest {
    @JsonProperty("transaction_type")
    @NotNull
    private TransactionType transactionType;

    @JsonProperty("amount")
    @NotNull
    private Double amount;

    @JsonProperty("debit_account_id")
    @NotNull
    private String debitAccountId;

    @JsonProperty("credit_account_id")
    @NotNull
    private String creditAccountId;
}
