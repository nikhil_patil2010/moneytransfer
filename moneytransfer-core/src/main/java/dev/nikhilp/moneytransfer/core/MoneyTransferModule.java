package dev.nikhilp.moneytransfer.core;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import dev.nikhilp.moneytransfer.core.dal.dao.AccountDAO;
import dev.nikhilp.moneytransfer.core.dal.dao.TransactionDAO;
import dev.nikhilp.moneytransfer.core.dal.dao.UserDAO;
import dev.nikhilp.moneytransfer.core.dal.dao.mysqlImpl.AccountDAOMysqlImpl;
import dev.nikhilp.moneytransfer.core.dal.dao.mysqlImpl.TransactionDAOMysqlImpl;
import dev.nikhilp.moneytransfer.core.dal.dao.mysqlImpl.UserDAOMysqlImpl;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Environment;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

public class MoneyTransferModule extends AbstractModule {

    private final MoneyTransferConfiguration moneyTransferConfiguration;
    private final Environment environment;

    @Inject
    public MoneyTransferModule(MoneyTransferConfiguration moneyTransferConfiguration, Environment environment) {
        this.moneyTransferConfiguration = moneyTransferConfiguration;
        this.environment = environment;
    }

    @Override
    protected void configure() {
        bind(MoneyTransferConfiguration.class).toInstance(moneyTransferConfiguration);
        bind(Environment.class).toInstance(environment);
        configureDAO();
    }

    private void configureDAO() {
        bind(UserDAO.class).to(UserDAOMysqlImpl.class);
        bind(AccountDAO.class).to(AccountDAOMysqlImpl.class);
        bind(TransactionDAO.class).to(TransactionDAOMysqlImpl.class);
    }

    @Provides
    @Singleton
    public DBI jdbiProvider(Environment environment, MoneyTransferConfiguration moneyTransferConfiguration) {
        DBIFactory jdbiFactory = new DBIFactory();
        DBI jdbi =jdbiFactory.build(environment, moneyTransferConfiguration.getDatabase(), "master");
        Handle handle = jdbi.open();

        handle.execute("CREATE TABLE users (id varchar(255) primary key, name varchar(255), date_of_birth date(6), " +
                "gender varchar(255), address varchar(255), national_id varchar(255), " +
                "created_at TIMESTAMP, updated_at TIMESTAMP)");

        handle.execute("CREATE TABLE accounts (account_id varchar(255) primary key, user_id varchar(255), " +
                "account_type varchar(255), account_status varchar(255), account_balance varchar(255), " +
                "created_at TIMESTAMP, updated_at TIMESTAMP)");

        handle.execute("CREATE TABLE transactions (transaction_id varchar(255), transaction_type varchar(255), " +
                "amount float, debit_account_id varchar(255), credit_account_id varchar(255), status varchar(255), " +
                "response_code varchar(255), created_at TIMESTAMP, updated_at TIMESTAMP);");
        jdbi.close(handle);
        return jdbi;
    }
}
