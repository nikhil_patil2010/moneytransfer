package dev.nikhilp.moneytransfer.core.models.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import dev.nikhilp.moneytransfer.core.models.data.Gender;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserRequest {
    @JsonProperty("name")
    private String name;

    @JsonProperty("date_of_birth")
    private Date dateOfBirth;

    @JsonProperty("gender")
    private Gender gender;

    @JsonProperty("address")
    private String address;

    @JsonProperty("national_id")
    private String nationalId;
}
