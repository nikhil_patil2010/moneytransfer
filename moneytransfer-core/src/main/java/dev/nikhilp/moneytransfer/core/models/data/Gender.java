package dev.nikhilp.moneytransfer.core.models.data;

public enum Gender {
    MALE,
    FEMALE,
    OTHERS
}
