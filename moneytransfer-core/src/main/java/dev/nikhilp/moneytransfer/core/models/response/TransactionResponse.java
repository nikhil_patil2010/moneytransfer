package dev.nikhilp.moneytransfer.core.models.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import dev.nikhilp.moneytransfer.core.models.data.TransactionResponseCode;
import dev.nikhilp.moneytransfer.core.models.data.TransactionStatus;
import dev.nikhilp.moneytransfer.core.models.data.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransactionResponse {
    @JsonProperty("transction_id")
    private String transactionId;

    @JsonProperty("transaction_type")
    private TransactionType transactionType;

    @JsonProperty("amount")
    private Double amount;

    @JsonProperty("debit_account_id")
    private String debitAccountId;

    @JsonProperty("credit_account_id")
    private String creditAccountId;

    @JsonProperty("status")
    private TransactionStatus status;

    @JsonProperty("response_code")
    private TransactionResponseCode responseCode;
}
