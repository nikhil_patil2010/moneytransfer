package dev.nikhilp.moneytransfer.core.executors.transaction;

import com.google.inject.Inject;
import dev.nikhilp.moneytransfer.core.dal.dao.AccountDAO;
import dev.nikhilp.moneytransfer.core.dal.dao.TransactionDAO;
import dev.nikhilp.moneytransfer.core.dal.transaction.DBContext;
import dev.nikhilp.moneytransfer.core.dal.transaction.DBTransactionHelper;
import dev.nikhilp.moneytransfer.core.exceptions.TransactionException;
import dev.nikhilp.moneytransfer.core.models.data.Account;
import dev.nikhilp.moneytransfer.core.models.data.Transaction;
import dev.nikhilp.moneytransfer.core.models.data.TransactionResponseCode;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class AccountTransferTransactionExecutor extends TransactionExecutor {
    private final AccountDAO accountDAO;

    @Inject
    public AccountTransferTransactionExecutor(DBTransactionHelper dBTransactionHelper, TransactionDAO transactionDAO,
                                              AccountDAO accountDAO) {
        super(dBTransactionHelper, transactionDAO);
        this.accountDAO = accountDAO;
    }

    @Override
    protected void process(DBContext dbContext, Transaction transaction) throws TransactionException {
        List<Account> accounts = accountDAO.selectAccountsForUpdate(
                Arrays.asList(transaction.getCreditAccountId(), transaction.getDebitAccountId()), dbContext.getSession());

        Account creditAccount = getAccount(transaction.getCreditAccountId(), accounts);
        Account debitAccount = getAccount(transaction.getDebitAccountId(), accounts);

        if(debitAccount.getAccountBalance() < transaction.getAmount()){
            throw new TransactionException("Insufficient balance in source account", TransactionResponseCode.INSUFFICIENT_BALANCE);
        }

        updateAccountBalance(creditAccount, creditAccount.getAccountBalance() + transaction.getAmount(), dbContext);
        updateAccountBalance(debitAccount, debitAccount.getAccountBalance() - transaction.getAmount(), dbContext);
    }

    private void updateAccountBalance(Account account, Double amount, DBContext dbContext) {
        account.setAccountBalance(amount);
        accountDAO.updateAccountBalance(account, dbContext.getSession());
    }

    private Account getAccount(String accountId, List<Account> accounts) {
        Account account = accounts.stream()
                .filter(_account -> _account.getAccountId().equals(accountId))
                .findFirst().orElse(null);

        if(Objects.isNull(account)) {
            throw new TransactionException("Account Doesn't exist", TransactionResponseCode.ACCOUNT_DOES_NOT_EXIST);
        }
        return account;
    }
}
