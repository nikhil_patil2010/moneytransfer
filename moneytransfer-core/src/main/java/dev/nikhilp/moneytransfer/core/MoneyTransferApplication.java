package dev.nikhilp.moneytransfer.core;

import com.google.inject.Guice;
import com.google.inject.Injector;
import dev.nikhilp.moneytransfer.core.resources.AccountResource;
import dev.nikhilp.moneytransfer.core.resources.TransactionResource;
import dev.nikhilp.moneytransfer.core.resources.UserResource;
import io.dropwizard.Application;
import io.dropwizard.jersey.jackson.JsonProcessingExceptionMapper;
import io.dropwizard.setup.Environment;

public class MoneyTransferApplication extends Application<MoneyTransferConfiguration> {

    public static void main(String[] args) throws Exception {
        new MoneyTransferApplication().run(args);
    }

    @Override
    public void run(MoneyTransferConfiguration moneyTransferConfiguration, Environment environment) throws Exception {
        Injector injector = Guice.createInjector(new MoneyTransferModule(moneyTransferConfiguration, environment));
        environment.jersey().register(injector.getInstance(AccountResource.class));
        environment.jersey().register(injector.getInstance(TransactionResource.class));
        environment.jersey().register(injector.getInstance(UserResource.class));
        environment.jersey().register(new JsonProcessingExceptionMapper(true));
    }

    @Override
    public String getName() {
        return "MoneyTransfer";
    }
}
