package dev.nikhilp.moneytransfer.core.executors.transaction;

import dev.nikhilp.moneytransfer.core.dal.dao.TransactionDAO;
import dev.nikhilp.moneytransfer.core.dal.transaction.DBContext;
import dev.nikhilp.moneytransfer.core.dal.transaction.DBTransactionHelper;
import dev.nikhilp.moneytransfer.core.exceptions.TransactionException;
import dev.nikhilp.moneytransfer.core.models.data.Transaction;
import dev.nikhilp.moneytransfer.core.models.data.TransactionResponseCode;
import dev.nikhilp.moneytransfer.core.models.data.TransactionStatus;

public abstract class TransactionExecutor {

    private final DBTransactionHelper dBTransactionHelper;
    private final TransactionDAO transactionDAO;

    public TransactionExecutor(DBTransactionHelper dBTransactionHelper, TransactionDAO transactionDAO) {
        this.dBTransactionHelper = dBTransactionHelper;
        this.transactionDAO = transactionDAO;
    }

    public Transaction execute(Transaction transaction) {
        transaction.setStatus(TransactionStatus.INIT);
        transactionDAO.createTransaction(transaction);

        try {
            dBTransactionHelper.doTransaction(new DBContext(), dbContext -> {
                try{
                    process(dbContext, transaction);
                    transaction.setStatus(TransactionStatus.SUCCESS);
                    transaction.setResponseCode(TransactionResponseCode.SUCCESS);
                } catch (TransactionException e) {
                    transaction.setStatus(TransactionStatus.FAILED);
                    transaction.setResponseCode(e.getTransactionResponseCode());
                } finally {
                    transactionDAO.updateTransactionStatusAndResponseCode(transaction, dbContext.getSession());
                }
            });
        } catch (Exception e) {
            transaction.setStatus(TransactionStatus.FAILED);
            transaction.setResponseCode(TransactionResponseCode.SERVER_ERROR);
            transactionDAO.updateTransactionStatusAndResponseCode(transaction);
        }

        return transaction;
    }

    protected abstract void process(DBContext dbContext, Transaction transaction) throws TransactionException;
}
