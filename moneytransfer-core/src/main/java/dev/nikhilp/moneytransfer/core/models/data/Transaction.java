package dev.nikhilp.moneytransfer.core.models.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {
    @JsonProperty("transaction_id")
    private String transactionId;

    @JsonProperty("transaction_type")
    private TransactionType transactionType;

    @JsonProperty("amount")
    private Double amount;

    @JsonProperty("debit_account_id")
    private String debitAccountId;

    @JsonProperty("credit_account_id")
    private String creditAccountId;

    @JsonProperty("status")
    private TransactionStatus status;

    @JsonProperty("response_code")
    private TransactionResponseCode responseCode;

    @JsonProperty("created_at")
    private Date createdAt;

    @JsonProperty("updated_at")
    private Date updatedAt;
}
