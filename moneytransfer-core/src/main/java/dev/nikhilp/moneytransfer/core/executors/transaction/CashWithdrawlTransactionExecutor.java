package dev.nikhilp.moneytransfer.core.executors.transaction;

import com.google.inject.Inject;
import dev.nikhilp.moneytransfer.core.dal.dao.AccountDAO;
import dev.nikhilp.moneytransfer.core.dal.dao.TransactionDAO;
import dev.nikhilp.moneytransfer.core.dal.transaction.DBContext;
import dev.nikhilp.moneytransfer.core.dal.transaction.DBTransactionHelper;
import dev.nikhilp.moneytransfer.core.exceptions.TransactionException;
import dev.nikhilp.moneytransfer.core.models.data.Account;
import dev.nikhilp.moneytransfer.core.models.data.Transaction;
import dev.nikhilp.moneytransfer.core.models.data.TransactionResponseCode;

import java.util.Objects;

public class CashWithdrawlTransactionExecutor extends TransactionExecutor {

    private final AccountDAO accountDAO;

    @Inject
    public CashWithdrawlTransactionExecutor(DBTransactionHelper dBTransactionHelper, TransactionDAO transactionDAO,
                                            AccountDAO accountDAO) {
        super(dBTransactionHelper, transactionDAO);
        this.accountDAO = accountDAO;
    }

    @Override
    protected void process(DBContext dbContext, Transaction transaction) throws TransactionException {
        Account account = accountDAO.selectAccountForUpdate(transaction.getDebitAccountId(), dbContext.getSession());

        if(Objects.isNull(account)) {
            throw new TransactionException("Account Doesn't exist", TransactionResponseCode.ACCOUNT_DOES_NOT_EXIST);
        }

        if(account.getAccountBalance() < transaction.getAmount()){
            throw new TransactionException("Insufficient balance in source account", TransactionResponseCode.INSUFFICIENT_BALANCE);
        }
        updateAccountBalance(account, account.getAccountBalance() - transaction.getAmount(), dbContext);
    }

    private void updateAccountBalance(Account account, Double amount, DBContext dbContext) {
        account.setAccountBalance(amount);
        accountDAO.updateAccountBalance(account, dbContext.getSession());
    }
}
