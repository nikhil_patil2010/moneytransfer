package dev.nikhilp.moneytransfer.core.resources;

import com.google.inject.Inject;
import dev.nikhilp.moneytransfer.core.models.request.CreateAccountRequest;
import dev.nikhilp.moneytransfer.core.models.response.AccountResponse;
import dev.nikhilp.moneytransfer.core.service.AccountService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AccountResource {


    private final AccountService accountService;

    @Inject
    public AccountResource(AccountService accountService) {
        this.accountService = accountService;
    }

    @POST
    @Path("/create")
    public Response createAccount(CreateAccountRequest createAccountRequest) {
        AccountResponse accountResponse = accountService.createAccount(createAccountRequest);
        return Response.status(201).entity(accountResponse).build();
    }

    @GET
    @Path("/")
    public Response getAccounts() {
        return Response.status(200).entity(accountService.getAccounts()).build();
    }

    @GET
    @Path("/{account_id}")
    public Response getAccount(@PathParam("account_id") String accountId) {
        return Response.status(200).entity(accountService.getAccount(accountId)).build();
    }

}
