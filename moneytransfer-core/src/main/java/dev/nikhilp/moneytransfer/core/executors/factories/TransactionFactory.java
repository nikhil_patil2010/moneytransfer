package dev.nikhilp.moneytransfer.core.executors.factories;

import com.google.common.collect.Maps;
import com.google.inject.Inject;
import dev.nikhilp.moneytransfer.core.executors.transaction.AccountTransferTransactionExecutor;
import dev.nikhilp.moneytransfer.core.executors.transaction.CashDepositTransactionExecutor;
import dev.nikhilp.moneytransfer.core.executors.transaction.CashWithdrawlTransactionExecutor;
import dev.nikhilp.moneytransfer.core.executors.transaction.TransactionExecutor;
import dev.nikhilp.moneytransfer.core.models.data.TransactionType;

import java.util.Map;

public class TransactionFactory {
    private final Map<TransactionType, TransactionExecutor> transactionExecutorMap;

    @Inject
    public TransactionFactory(CashDepositTransactionExecutor cashDepositTransactionExecutor,
                              CashWithdrawlTransactionExecutor cashWithdrawlTransactionExecutor,
                              AccountTransferTransactionExecutor accountTransferTransactionExecutor) {
        transactionExecutorMap = Maps.newHashMap();
        transactionExecutorMap.put(TransactionType.CASH_DEPOSIT, cashDepositTransactionExecutor);
        transactionExecutorMap.put(TransactionType.CASH_WITHDRAWL, cashWithdrawlTransactionExecutor);
        transactionExecutorMap.put(TransactionType.ACCOUNT_TRANSFER, accountTransferTransactionExecutor);
    }

    public TransactionExecutor getTransactionExecutor(TransactionType transactionType) {
            return transactionExecutorMap.get(transactionType);
    }

}
