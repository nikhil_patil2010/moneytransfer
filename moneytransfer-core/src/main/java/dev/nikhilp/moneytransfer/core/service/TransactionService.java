package dev.nikhilp.moneytransfer.core.service;

import com.google.inject.Inject;
import dev.nikhilp.moneytransfer.core.dal.dao.TransactionDAO;
import dev.nikhilp.moneytransfer.core.executors.factories.TransactionFactory;
import dev.nikhilp.moneytransfer.core.executors.transaction.TransactionExecutor;
import dev.nikhilp.moneytransfer.core.models.data.Transaction;
import dev.nikhilp.moneytransfer.core.models.request.TransactionRequest;
import dev.nikhilp.moneytransfer.core.models.response.TransactionResponse;

import javax.ws.rs.BadRequestException;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

public class TransactionService {
    private final TransactionFactory transactionFactory;
    private final TransactionDAO transactionDAO;

    @Inject
    public TransactionService(TransactionFactory transactionFactory, TransactionDAO transactionDAO) {
        this.transactionFactory = transactionFactory;
        this.transactionDAO = transactionDAO;
    }

    public TransactionResponse execute(TransactionRequest transactionRequest) {
        validate(transactionRequest);
        Transaction transaction = adaptRequest(transactionRequest);
        transaction.setTransactionId(UUID.randomUUID().toString());

        TransactionExecutor transactionExecutor = transactionFactory.getTransactionExecutor(
                transactionRequest.getTransactionType());
        transaction = transactionExecutor.execute(transaction);

        return adaptToResponse(transaction);
    }

    private void validate(TransactionRequest transactionRequest) {
        if(Objects.isNull(transactionRequest.getAmount()) || transactionRequest.getAmount() < 0) {
            throw new BadRequestException("Amount is invalid");
        }
    }

    public List<TransactionResponse> get() {
        List<Transaction> transactions = transactionDAO.getTransactions();
        return transactions.stream().map(this::adaptToResponse).collect(Collectors.toList());
    }

    private Transaction adaptRequest(TransactionRequest transactionRequest) {
        return Transaction.builder().amount(transactionRequest.getAmount())
                .creditAccountId(transactionRequest.getCreditAccountId())
                .debitAccountId(transactionRequest.getDebitAccountId())
                .transactionType(transactionRequest.getTransactionType())
                .build();
    }

    private TransactionResponse adaptToResponse(Transaction transaction) {
        if(Objects.isNull(transaction)) return null;
        return TransactionResponse.builder().transactionId(transaction.getTransactionId())
                .amount(transaction.getAmount())
                .creditAccountId(transaction.getCreditAccountId())
                .debitAccountId(transaction.getDebitAccountId())
                .transactionType(transaction.getTransactionType())
                .responseCode(transaction.getResponseCode())
                .status(transaction.getStatus())
                .build();
    }
}
