package dev.nikhilp.moneytransfer.core.dal.transaction;

public interface ITransactionBlock {
    void execute(DBContext context) throws Exception;
}
