package dev.nikhilp.moneytransfer.core.models.data;

public enum AccountStatus {
    ACTIVE,
    INACTIVE
}
