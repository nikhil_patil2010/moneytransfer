package dev.nikhilp.moneytransfer.core.service;

import com.google.inject.Inject;
import dev.nikhilp.moneytransfer.core.dal.dao.AccountDAO;
import dev.nikhilp.moneytransfer.core.dal.transaction.DBContext;
import dev.nikhilp.moneytransfer.core.dal.transaction.ITransactionBlock;
import dev.nikhilp.moneytransfer.core.dal.transaction.DBTransactionHelper;
import dev.nikhilp.moneytransfer.core.models.data.Account;
import dev.nikhilp.moneytransfer.core.models.data.AccountStatus;
import dev.nikhilp.moneytransfer.core.models.request.CreateAccountRequest;
import dev.nikhilp.moneytransfer.core.models.response.AccountResponse;
import dev.nikhilp.moneytransfer.core.models.response.UserResponse;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.BadRequestException;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
public class AccountService {

    private final AccountDAO accountDAO;
    private final UserService userService;

    @Inject
    public AccountService(AccountDAO accountDAO, UserService userService) {
        this.accountDAO = accountDAO;
        this.userService = userService;
    }

    public AccountResponse createAccount(CreateAccountRequest createAccountRequest) {
        validateUser(createAccountRequest.getUserId());
        Account account = adaptRequest(createAccountRequest);
        setInitialAccountParams(account);
        accountDAO.createAccount(account);
        log.info("Account creation successful for request {}", createAccountRequest);
        return adaptToResponse(account);
    }

    private void validateUser(String userId) {
        UserResponse user = userService.getUser(userId);
        if(Objects.isNull(user)) {
            throw new BadRequestException("User doesn't exist");
        }
    }

    public AccountResponse getAccount(String accountId) {
        return adaptToResponse(accountDAO.getAccount(accountId));
    }

    public List<AccountResponse> getAccounts() {
        return accountDAO.getAccounts().stream().map(this::adaptToResponse).collect(Collectors.toList());
    }

    private void setInitialAccountParams(Account account) {
        account.setAccountId(generateAccountId());
        account.setAccountStatus(AccountStatus.ACTIVE);
        account.setAccountBalance(0D);
    }

    private Account adaptRequest(CreateAccountRequest createAccountRequest) {
        return Account.builder().userId(createAccountRequest.getUserId())
                .accountType(createAccountRequest.getAccountType())
                .build();
    }

    private AccountResponse adaptToResponse(Account account) {
        if(Objects.isNull(account)) return null;
        return AccountResponse.builder().accountId(account.getAccountId())
                .userId(account.getUserId())
                .accountStatus(account.getAccountStatus())
                .accountType(account.getAccountType())
                .accountBalance(account.getAccountBalance())
                .createdAt(account.getCreatedAt())
                .updatedAt(account.getUpdatedAt())
                .build();
    }

    private String generateAccountId() {
        return UUID.randomUUID().toString();
    }
}
