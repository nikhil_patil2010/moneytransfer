package dev.nikhilp.moneytransfer.core.dal.dao.handles;

import dev.nikhilp.moneytransfer.core.dal.dao.mappers.TransactionMapper;
import dev.nikhilp.moneytransfer.core.dal.sessionhandle.SessionHandle;
import dev.nikhilp.moneytransfer.core.models.data.Transaction;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.GetHandle;

import java.util.List;

public abstract class TransactionDAOHandle implements GetHandle, SessionHandle {

    @SqlUpdate("INSERT into transactions (transaction_id, transaction_type, amount, debit_account_id, credit_account_id, status, created_at, updated_at)" +
            " values (:transactionId, :transactionType, :amount, :debitAccountId, :creditAccountId, :status, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
    public abstract void createTransaction(@BindBean Transaction transaction);

    @SqlUpdate("UPDATE transactions SET status = :status, response_code = :responseCode, updated_at = CURRENT_TIMESTAMP WHERE transaction_id = :transactionId")
    public abstract void updateTransactionStatusAndResponseCode(@BindBean Transaction transaction);

    @SqlQuery("SELECT * FROM transactions LIMIT 10")
    @RegisterMapper(TransactionMapper.class)
    public abstract List<Transaction> getTransactions();

}
