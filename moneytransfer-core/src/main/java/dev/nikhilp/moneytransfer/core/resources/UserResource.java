package dev.nikhilp.moneytransfer.core.resources;

import com.google.inject.Inject;
import dev.nikhilp.moneytransfer.core.models.request.CreateAccountRequest;
import dev.nikhilp.moneytransfer.core.models.request.CreateUserRequest;
import dev.nikhilp.moneytransfer.core.models.response.AccountResponse;
import dev.nikhilp.moneytransfer.core.models.response.UserResponse;
import dev.nikhilp.moneytransfer.core.service.AccountService;
import dev.nikhilp.moneytransfer.core.service.UserService;
import org.h2.command.ddl.CreateUser;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {


    private final UserService userService;

    @Inject
    public UserResource(UserService userService) {
        this.userService = userService;
    }

    @POST
    @Path("/create")
    public Response createUser(CreateUserRequest createUserRequest) {
        UserResponse userResponse = userService.createUser(createUserRequest);
        return Response.status(201).entity(userResponse).build();
    }

    @GET
    @Path("/")
    public Response getUsers() {
        return Response.status(200).entity(userService.getUsers()).build();
    }

    @GET
    @Path("/{user_id}")
    public Response getAccount(@PathParam("user_id") String id) {
        return Response.status(200).entity(userService.getUser(id)).build();
    }

}
