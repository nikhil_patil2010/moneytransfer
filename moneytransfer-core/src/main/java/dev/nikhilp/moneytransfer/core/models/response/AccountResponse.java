package dev.nikhilp.moneytransfer.core.models.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import dev.nikhilp.moneytransfer.core.models.data.AccountStatus;
import dev.nikhilp.moneytransfer.core.models.data.AccountType;
import lombok.*;

import java.util.Date;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AccountResponse {
    @JsonProperty("account_id")
    private String accountId;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("account_type")
    private AccountType accountType;

    @JsonProperty("account_status")
    private AccountStatus accountStatus;

    @JsonProperty("account_balance")
    private Double accountBalance;

    @JsonProperty("created_at")
    private Date createdAt;

    @JsonProperty("updated_at")
    private Date updatedAt;
}
