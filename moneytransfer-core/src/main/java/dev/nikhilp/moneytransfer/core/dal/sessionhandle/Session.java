package dev.nikhilp.moneytransfer.core.dal.sessionhandle;

public interface Session {
    <SessionHandler extends SessionHandle> SessionHandler getSessionHandle(Class<SessionHandler> sessionHandleClass);

    <ConnectionHandler> ConnectionHandler getConnectionHandler();
}
