package dev.nikhilp.moneytransfer.core.dal.dao;

import dev.nikhilp.moneytransfer.core.dal.sessionhandle.Session;
import dev.nikhilp.moneytransfer.core.models.data.Account;

import java.util.List;

public interface AccountDAO {
    void createAccount(Account account);

    Account getAccount(String accountId);

    Account selectAccountForUpdate(String accountId, Session session);

    List<Account> selectAccountsForUpdate(List<String> accountIds, Session session);

    void updateAccountBalance(Account account, Session session);

    List<Account> getAccounts();
}
