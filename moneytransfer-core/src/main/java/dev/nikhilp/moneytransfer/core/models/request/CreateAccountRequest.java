package dev.nikhilp.moneytransfer.core.models.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import dev.nikhilp.moneytransfer.core.models.data.AccountType;
import dev.nikhilp.moneytransfer.core.models.data.Gender;
import lombok.Data;

import java.util.Date;

@Data
public class CreateAccountRequest {
    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("account_type")
    private AccountType accountType;
}
