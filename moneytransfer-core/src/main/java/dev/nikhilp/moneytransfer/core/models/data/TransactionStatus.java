package dev.nikhilp.moneytransfer.core.models.data;

public enum TransactionStatus {
    INIT,
    PROCESSING,
    SUCCESS,
    FAILED
}
