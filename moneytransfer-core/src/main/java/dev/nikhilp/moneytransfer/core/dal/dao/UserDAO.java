package dev.nikhilp.moneytransfer.core.dal.dao;

import dev.nikhilp.moneytransfer.core.models.data.User;

import java.util.List;

public interface UserDAO {
    void createUser(User user);

    User getUser(String userId);

    List<User> getUsers();
}
