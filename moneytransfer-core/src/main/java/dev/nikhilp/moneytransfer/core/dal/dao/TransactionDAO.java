package dev.nikhilp.moneytransfer.core.dal.dao;

import dev.nikhilp.moneytransfer.core.dal.sessionhandle.Session;
import dev.nikhilp.moneytransfer.core.models.data.Transaction;

import java.util.List;

public interface TransactionDAO {
    void createTransaction(Transaction transaction);

    void updateTransactionStatusAndResponseCode(Transaction transaction, Session session);

    void updateTransactionStatusAndResponseCode(Transaction transaction);

    List<Transaction> getTransactions();
}
