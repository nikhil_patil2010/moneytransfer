package dev.nikhilp.moneytransfer.core.dal.dao.mappers;

import dev.nikhilp.moneytransfer.core.models.data.Account;
import dev.nikhilp.moneytransfer.core.models.data.AccountStatus;
import dev.nikhilp.moneytransfer.core.models.data.AccountType;
import dev.nikhilp.moneytransfer.core.models.data.Gender;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountMapper implements ResultSetMapper<Account> {
    @Override
    public Account map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return Account.builder().accountId(resultSet.getString("account_id"))
                .userId(resultSet.getString("user_id"))
                .accountStatus(AccountStatus.valueOf(resultSet.getString("account_status")))
                .accountType(AccountType.valueOf(resultSet.getString("account_type")))
                .accountBalance(resultSet.getDouble("account_balance"))
                .createdAt(resultSet.getTimestamp("created_at"))
                .updatedAt(resultSet.getTimestamp("updated_at"))
                .build();
    }
}
