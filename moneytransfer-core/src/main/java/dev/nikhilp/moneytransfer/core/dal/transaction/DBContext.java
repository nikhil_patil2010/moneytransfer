package dev.nikhilp.moneytransfer.core.dal.transaction;

import dev.nikhilp.moneytransfer.core.dal.sessionhandle.Session;
import lombok.Data;

@Data
public class DBContext {
    Session session;
}
