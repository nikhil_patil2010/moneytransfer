package dev.nikhilp.moneytransfer.core.dal.dao.mysqlImpl;

import dev.nikhilp.moneytransfer.core.dal.JdbiMysqlDAO;
import dev.nikhilp.moneytransfer.core.dal.dao.UserDAO;
import dev.nikhilp.moneytransfer.core.dal.dao.handles.UserDAOHandle;
import dev.nikhilp.moneytransfer.core.models.data.User;

import java.util.List;

public class UserDAOMysqlImpl extends JdbiMysqlDAO<UserDAOHandle> implements UserDAO {
    @Override
    public void createUser(User user) {
        getSessionHandleOnDemand().createUser(user);
    }

    @Override
    public User getUser(String userId) {
        return getSessionHandleOnDemand().getUser(userId);
    }

    @Override
    public List<User> getUsers() {
        return getSessionHandleOnDemand().getUsers();
    }
}
