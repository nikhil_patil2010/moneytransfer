package dev.nikhilp.moneytransfer.core.models.data;

public enum TransactionResponseCode {
    SUCCESS,
    ACCOUNT_DOES_NOT_EXIST,
    INSUFFICIENT_BALANCE,
    SERVER_ERROR
}
