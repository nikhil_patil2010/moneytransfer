package dev.nikhilp.moneytransfer.core.dal.sessionhandle;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool.PoolableObjectFactory;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

import java.sql.SQLException;

@Singleton
@Slf4j
public class MasterDBSessionConnectionPool implements PoolableObjectFactory<DBSession> {

    private final DBI jdbi;

    @Inject
    public MasterDBSessionConnectionPool(DBI jdbi) {
        this.jdbi = jdbi;
    }

    @Override
    public DBSession makeObject() throws Exception {
        Handle masterHandle = jdbi.open();
        return new DBSession(masterHandle);
    }

    @Override
    public void destroyObject(DBSession dbSession) throws Exception {
        dbSession.<Handle>getConnectionHandler().close();
    }

    @Override
    public boolean validateObject(DBSession dbSession) {
        try {
            return (!dbSession.<Handle>getConnectionHandler().getConnection().isClosed()) &&
                    dbSession.<Handle>getConnectionHandler().getConnection().isValid(2);
        } catch (SQLException e) {
            log.error("ValidateObject failed", e);
            return false;
        }
    }

    @Override
    public void activateObject(DBSession dbSession) throws Exception {

    }

    @Override
    public void passivateObject(DBSession dbSession) throws Exception {

    }
}
