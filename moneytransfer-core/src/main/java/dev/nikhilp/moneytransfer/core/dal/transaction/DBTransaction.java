package dev.nikhilp.moneytransfer.core.dal.transaction;

import dev.nikhilp.moneytransfer.core.dal.sessionhandle.Session;
import lombok.extern.slf4j.Slf4j;
import org.skife.jdbi.v2.Handle;
import org.skife.jdbi.v2.exceptions.DBIException;

@Slf4j
public class DBTransaction {

    protected void startTransaction(final Session session) {
        int retry = 0;
        boolean failed = true;
        while(failed && retry <= 3){
            try {
                session.<Handle>getConnectionHandler().begin();
                failed = false;
            } catch (DBIException e) {
                log.error("Error in starting SQL transaction",e);
                if(retry == 3) {
                    session.<Handle>getConnectionHandler().close();
                    throw new RuntimeException("Exception while starting transaction");
                } else {
                    retry += 1;
                }
            }
        }
    }

    protected void commit(final Session session) {
        int retry = 0;
        boolean failed = true;
        while(failed && retry <= 3){
            try {
                session.<Handle>getConnectionHandler().commit();
                failed = false;
            } catch (DBIException e) {
                log.error("Error in starting SQL transaction",e);
                if(retry == 3) {
                    session.<Handle>getConnectionHandler().close();
                    throw new RuntimeException("Exception while committing transaction");
                } else {
                    retry += 1;
                }
            }
        }
    }

    protected void rollback(final Session session) {
        int retry = 0;
        boolean failed = true;
        while(failed && retry <= 3){
            try {
                session.<Handle>getConnectionHandler().rollback();
                failed = false;
            } catch (DBIException e) {
                log.error("Error in starting SQL transaction",e);
                if(retry == 3) {
                    session.<Handle>getConnectionHandler().close();
                    throw new RuntimeException("Exception while rolling back transaction");
                } else {
                    retry += 1;
                }
            }
        }
    }
}
