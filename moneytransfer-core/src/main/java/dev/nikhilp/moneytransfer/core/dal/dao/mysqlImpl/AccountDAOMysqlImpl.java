package dev.nikhilp.moneytransfer.core.dal.dao.mysqlImpl;

import dev.nikhilp.moneytransfer.core.dal.JdbiMysqlDAO;
import dev.nikhilp.moneytransfer.core.dal.dao.AccountDAO;
import dev.nikhilp.moneytransfer.core.dal.dao.handles.AccountDAOHandle;
import dev.nikhilp.moneytransfer.core.dal.sessionhandle.Session;
import dev.nikhilp.moneytransfer.core.models.data.Account;

import java.util.List;

public class AccountDAOMysqlImpl extends JdbiMysqlDAO<AccountDAOHandle> implements AccountDAO {
    @Override
    public void createAccount(Account account) {
        getSessionHandleOnDemand().createAccount(account);
    }

    @Override
    public Account getAccount(String accountId) {
        return getSessionHandleOnDemand().getAccount(accountId);
    }

    @Override
    public Account selectAccountForUpdate(String accountId, Session session) {
        return getSessionHandleFromSession(session).selectAccountForUpdate(accountId);
    }

    @Override
    public List<Account> selectAccountsForUpdate(List<String> accountIds, Session session) {
        return getSessionHandleFromSession(session).selectAccountsForUpdate(accountIds);
    }

    public void updateAccountBalance(Account account, Session session) {
        getSessionHandleFromSession(session).updateAccountBalance(account);
    }

    public List<Account> getAccounts() {
        return getSessionHandleOnDemand().getAccounts();
    }
}
