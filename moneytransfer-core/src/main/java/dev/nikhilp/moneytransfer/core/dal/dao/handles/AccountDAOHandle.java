package dev.nikhilp.moneytransfer.core.dal.dao.handles;

import dev.nikhilp.moneytransfer.core.dal.dao.mappers.AccountMapper;
import dev.nikhilp.moneytransfer.core.dal.sessionhandle.SessionHandle;
import dev.nikhilp.moneytransfer.core.models.data.Account;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.helpers.MapResultAsBean;
import org.skife.jdbi.v2.sqlobject.mixins.GetHandle;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.unstable.BindIn;

import java.util.List;

@UseStringTemplate3StatementLocator
public abstract class AccountDAOHandle implements GetHandle, SessionHandle {

    @SqlUpdate("INSERT into accounts (account_id, user_id, account_type, account_status, account_balance, created_at, updated_at)" +
            " values (:accountId, :userId, :accountType, :accountStatus, :accountBalance, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
    public abstract void createAccount(@BindBean Account account);

    @SqlQuery("SELECT * FROM accounts WHERE account_id = :account_id")
    @RegisterMapper(AccountMapper.class)
    public abstract Account getAccount(@Bind("account_id") String id);

    @SqlQuery("SELECT * FROM accounts LIMIT 10")
    @RegisterMapper(AccountMapper.class)
    public abstract List<Account> getAccounts();

    @SqlQuery("SELECT * FROM accounts WHERE account_id = :account_id FOR UPDATE")
    @RegisterMapper(AccountMapper.class)
    public abstract Account selectAccountForUpdate(@Bind("account_id") String accountId);

    @SqlQuery("SELECT * FROM accounts WHERE account_id IN (<account_ids>) FOR UPDATE")
    @RegisterMapper(AccountMapper.class)
    public abstract List<Account> selectAccountsForUpdate(@BindIn("account_ids") List<String> accountIds);

    @SqlUpdate("UPDATE accounts SET account_balance = :accountBalance WHERE account_id = :accountId")
    public abstract void updateAccountBalance(@BindBean Account account);
}
