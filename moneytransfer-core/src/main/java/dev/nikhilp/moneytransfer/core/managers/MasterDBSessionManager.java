package dev.nikhilp.moneytransfer.core.managers;

import com.google.inject.Inject;
import dev.nikhilp.moneytransfer.core.dal.sessionhandle.DBSession;
import dev.nikhilp.moneytransfer.core.dal.sessionhandle.MasterDBSessionConnectionPool;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool.impl.GenericObjectPool;

import java.util.Objects;

@Slf4j
public class MasterDBSessionManager {
    private static GenericObjectPool<DBSession> dbSessionGenericObjectPool;

    @Inject
    public MasterDBSessionManager(MasterDBSessionConnectionPool masterDBSessionConnectionPool) {
        dbSessionGenericObjectPool = new GenericObjectPool<>(masterDBSessionConnectionPool);
        dbSessionGenericObjectPool.setTestOnBorrow(true);
        dbSessionGenericObjectPool.setLifo(false);
        dbSessionGenericObjectPool.setMaxIdle(1);
        dbSessionGenericObjectPool.setMinEvictableIdleTimeMillis(1000*60*60*24);
        dbSessionGenericObjectPool.setTimeBetweenEvictionRunsMillis(1000*60*60*24);
        dbSessionGenericObjectPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_BLOCK);
    }

    public DBSession getMasterDBSession() {
        try {
            return dbSessionGenericObjectPool.borrowObject();
        } catch (Exception e) {
            log.error("Exception while fetching db session", e);
            throw new RuntimeException("Exception while fetching db session", e);
        }
    }

    public void returnDBSession(DBSession masterDbSession) {
        try {
            if(Objects.nonNull(masterDbSession)) {
                dbSessionGenericObjectPool.returnObject(masterDbSession);
            }
        } catch (Exception e) {
            log.error("Exception while release db session");
        }
    }
}
